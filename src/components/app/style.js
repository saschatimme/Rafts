import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#333333",
  },
  header: {
    height: 22,
    backgroundColor: "#000000",
  },
  input: {
    backgroundColor: "#222222",
    color: "#bbbbbb",
    fontSize: 16,
    height: 284,
  },
  menu: {
    backgroundColor: "#333333",
    height: 40,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  list: {
    backgroundColor: "blue",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },

  instructions: {
    textAlign: "center",
    color: "#BBBBBB",
    marginBottom: 5,
  },
});

export default styles;
