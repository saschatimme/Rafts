import React, { Component } from "react";
import { Keyboard, StatusBar, StyleSheet, Text, TextInput, View } from "react-native";

import UselessTextInput from "../textedit/index";
import Menu from "../menu/index";
import List from "../list/index";
import styles from "./style.js";

const TextRef = "MyTextField";

const propTypes = {
  showMqttClientStatus: React.PropTypes.bool.isRequired,
};

// TODO: sascha abklären, ob das der beste weg ist^^
function addNote(state) {
  const macId = state.notes.reduce((acc, cur) => acc.id > cur.id ? acc : cur, { id: 0 });
  const newNote = { text: "new", id: macId + 1, lastAccess: new Date().getTime() };
  let newNotes = [...state.notes, newNote];
  return { notes: newNotes };
}

// TODO ebenso als function?
class App extends Component {
  static propTypes = {
    modi: React.PropTypes.oneOf(["edit", "pub", "priv"]),
  };

  static defaultProps = {
    modi: "pub",
  };

  constructor(props) {
    super(props);
    this._updateNote = this._updateNote.bind(this);
    this._addNote = this._addNote.bind(this);

    this.state = {
      // currentNoteId: 1,
      // currentNoteId: 0,
      modi: props.modi,
      notes: [
        { text: "text1", id: 1, lastAccess: 520239423 },
        { text: "text2", id: 5, lastAccess: 534239423 },
        { text: "text 1234", id: 3, lastAccess: 52023129423 },
        { text: "text first", id: 8, lastAccess: 99023129423 },
      ],
    };
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", e => this._keyboardDidShow(e));
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", e => this._keyboardDidHide(e));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.modi !== "edit" && this.state.modi === "edit") {
      this.textRef.focus();
    }
  }

  _keyboardDidShow() {
    console.log("_keyboardDidShow");
  }

  _keyboardDidHide() {
    console.log("_keyboardDidHide");
    if (this.state.modi !== "pub") {
      this.setState({ modi: "pub" });
    }
  }

  _inputOnFocus = () => {
    console.log("_inputOnFocus");
    if (this.state.modi !== "edit") {
      this.setState({ modi: "edit" });
    }
  };

  _updateNote(currentNoteText) {
    console.log("currentNote", currentNoteText);

    // let b = { a: "1" };
    // let c = b; //;{ ...b };
    // b.a = "2";
    // console.log("b", b);
    // console.log("c", c);

    const currentNote = { ...this.state.notes[0], text: currentNoteText, lastAccess: new Date().getTime() };
    const newNotes = [currentNote, ...this.state.notes.slice(1)];
    this.setState({ notes: newNotes });
  }

  // TODO: sascha abklären, ob das der beste weg ist^^
  _addNote = () => {
    this.setState(addNote);
  };
  _selectNote = noteId => {
    const indexNotes = this.state.notes.map((val, index) => ({ ...val, index }));
    const selectedNote = indexNotes.reduce((acc, cur) => acc.id === noteId ? acc : cur, { text: "" });
    selectedNote.lastAccess = new Date().getTime();
    const newNotes = Object.assign([], indexNotes, { [selectedNote.index]: selectedNote });
    this.setState({ notes: newNotes, modi: "edit" });

    console.log("_focusNextField", this.refs);^
    this.textRef.focus();
  };

  _toggleKeyboard = () => {
    console.log("_toggleKeyboard", this.state.modi);
    if (this.state.modi !== "edit") {
      this.setState({ modi: "edit" });
      this.textRef.focus();
    } else {
      this.setState({ modi: "pub" });
      Keyboard.dismiss();
    }
  };

  render() {
    const { modi, notes } = this.state;

    // WHY CAN'T I SET THE KEYBOARD_FOCUS HERE???
    if (modi !== "edit") {
      console.log("dismiss keyboard", modi);
      // Keyboard.dismiss();
    } else {
    }
    const notesSorted = notes.sort(({ lastAccess: a }, { lastAccess: b }) => b - a);
    const notesText = notesSorted;
    console.log("render", modi);
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#000000" barStyle="light-content" />
        <View style={styles.header} />
        <TextInput
          ref={x => {
            this.textRef = x;
          })}
          style={styles.input}
          autoFocus={modi === "edit"}
          editable={true}
          maxLength={40}
          multiline={true}
          numberOfLines={4}
          onChangeText={this._updateNote}
          onFocus={this._inputOnFocus}
          value={notesSorted[0].text}
        />
        <Menu style={styles.menu} action={[this._addNote, this._toggleKeyboard]} />
        <List style={styles.list} data={notesText} selectAction={this._selectNote} />
      </View>
    );
  }
}

export default App;
