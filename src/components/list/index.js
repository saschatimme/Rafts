import React from "react";
import {
  // Button,
  StyleSheet,
  ListView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#123456",
    width: 40,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: "#8E8E8E",
  },
  row: {
    padding: 8,
    height: 40,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#111111",
  },
  rowText: {
    color: "#cccccc",
  },
});

// ##
/*const MyList = ({ ...rest, text }) => {

  return (
    <ListView
      dataSource={this.state.dataSource}
      renderRow={(rowData) => <View style={styles.row}><Text style={styles.rowText}>{rowData}</Text></View>}
      renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
    />
  );
};*/

const x = function() {
  console.log("x");
};

//var ListViewSimpleExample = React.createClass({
class MyComponent extends React.Component {
  static propTypes = {
    data: React.PropTypes.array,
  };

  constructor(props) {
    console.log("constructor", props);
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = { dataSource: ds.cloneWithRows(props.data) };

    // this.selectItem = this.selectItem.bind(this);
  }

  componentWillUpdate(props) {
    // console.log('componentWillUpdate');
  }

  componentWillReceiveProps({ data: newData }) {
    const oldData = this.props.data;
    if (newData !== oldData) {
      const dataSource = this.state.dataSource.cloneWithRows(newData || []);
      this.setState({ dataSource });
    }
  }

  // selectItem = (noteId) => {
  //   console.log("select", noteId);
  // }

  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={rowData => (
          <TouchableOpacity value={5} onPress={() => this.props.selectAction(rowData.id)}>
            <View value={rowData.id} style={styles.row}>
              <Text style={styles.rowText}>{rowData.text}</Text>
            </View>
          </TouchableOpacity>
        )}
        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
      />
    );
  }
}

export default MyComponent;
