import React, { Component } from "react";
import {
  // StyleSheet,
  TextInput,
} from "react-native";

class MyTextInput extends Component {
  // function MyTextInput(props) {
  constructor(props) {
    super(props);
  }

  render() {
    return <TextInput {...this.props} editable={true} maxLength={40} />;
  }

  // return <TextInput {...props} editable={true} maxLength={40} />;
}
export default MyTextInput;
