import React from "react";
import {
  // Button,
  StyleSheet,
  Text,
  View,
} from "react-native";
import Button from "react-native-button";

import Button2 from "../button/index";

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#123456",
    width: 40,
    // fontSize: 20,
    // height: 30,
  },
});

const onPressLearnMore = () => {
  console.log("test");
};

const style = {
  width: 40,
  height: 40,
  marginRight: 5,
  paddingTop: 11,
  fontSize: 13,
  color: "white",
  backgroundColor: "#555555",
};

function Menu(props) {
  return (
    // <View {...props}>
    (
      <View {...props}>
        <View style={{ flexDirection: "row" }}>
          <Button style={style} onPress={props.action[0]}>Add</Button>
          <Button style={style} onPress={onPressLearnMore}>Pub</Button>
          <Button style={style} onPress={onPressLearnMore}>Pri</Button>
        </View>
        <Button style={style} onPress={props.action[1]}>v</Button>
      </View>
    )
  );
}
export default Menu;
