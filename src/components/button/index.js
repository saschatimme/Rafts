
import React, { PropTypes } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
} from 'react-native';
// import Button from 'react-native-button';

const onPressLearnMore = () => {
  console.log('test');
};

const styles = StyleSheet.create({
  but: {
    fontSize: 30,
    color: '#ffffff',
  },
});

const MyButton = ({ ...rest, text }) => {
  return (
    <View>
      <Button
        {... rest}
        onPress={onPressLearnMore}
        title={text}
        color="#987654"
        backgroundColor="#CCCCCC"
        accessibilityLabel="Learn more about this purple button"
        style={{ fontSize: 20, color: 'green' }}
      />
    </View>
  );
};

MyButton.propTypes = {
  text: PropTypes.string.isRequired,
};

export default MyButton;
